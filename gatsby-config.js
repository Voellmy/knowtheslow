module.exports = {
  siteMetadata: {
    title: `Player Timer`,
    description: `Player timer to keep track of which player used how much time.`,
    author: `Raphael Voellmy`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Know The Slow`,
        short_name: `KnowTheSlow`,
        start_url: `/`,
        background_color: `#2c3e50`,
        theme_color: `#2c3e50`,
        display: `standalone`,
        icon: `src/images/favicon.svg`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-elm`,
    `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-gtag`,
      options: {
        trackingId: process.env.GA_TRACKING_ID,
        anonymize: true,
      },
    },
  ],
}