module Main exposing (Msg(..), main, update, view)

import Browser
import Dict exposing (Dict)
import Html exposing (Html, button, div, text)
import Html.Attributes exposing (class, style)
import Html.Events exposing (onClick)
import Time


type Player
    = Player String


availablePlayers : List Player
availablePlayers =
    [ Player "black"
    , Player "white"
    , Player "red"
    , Player "blue"
    , Player "green"
    , Player "yellow"
    , Player "violet"
    , Player "orange"
    ]


type alias Model =
    { players : List Player
    , activePlayer : Maybe Player
    , started : Bool
    , times : Dict String Int
    }


initialModel : Model
initialModel =
    { players = []
    , activePlayer = Nothing
    , started = False
    , times = Dict.empty
    }


main =
    Browser.element
        { init = \() -> ( initialModel, Cmd.none )
        , update = \msg model -> ( update msg model, Cmd.none )
        , view = view
        , subscriptions = \_ -> Time.every 1000 (\_ -> Tick)
        }


type Msg
    = Tick
    | SelectPlayer Player
    | SetActivePlayer Player
    | StartGame
    | Pause


update : Msg -> Model -> Model
update msg model =
    case msg of
        Tick ->
            case model.activePlayer of
                Nothing ->
                    model

                Just (Player color) ->
                    { model
                        | times =
                            Dict.update color
                                (\time ->
                                    case time of
                                        Nothing ->
                                            Just 1

                                        Just seconds ->
                                            Just (seconds + 1)
                                )
                                model.times
                    }

        SelectPlayer player ->
            if List.member player model.players then
                -- remove player
                { model | players = List.filter (\p -> p /= player) model.players }

            else
                -- add player
                { model | players = player :: model.players }

        SetActivePlayer player ->
            case model.activePlayer of
                Just currentPlayer ->
                    { model
                        | activePlayer =
                            if currentPlayer == player then
                                Nothing

                            else
                                Just player
                    }

                Nothing ->
                    { model | activePlayer = Just player }

        Pause ->
            { model | activePlayer = Nothing }

        StartGame ->
            { model | started = True }


view : Model -> Html Msg
view model =
    div
        [ style "display" "flex"
        , style "flex-direction" "column"
        , style "justify-content" "space-around"
        , style "min-height" "100vh"
        ]
        [ if model.started then
            clock model

          else
            chosePlayer model
        ]


chosePlayer : Model -> Html Msg
chosePlayer model =
    div []
        [ div
            [ style "font-size" "2rem"
            , style "margin" "2rem"
            , style "text-align" "center"
            ]
            [ text "Choose Colors..." ]
        , div
            [ class "grid"
            ]
            (List.map
                (\player ->
                    let
                        buttonClass =
                            "player-select-circle "
                                ++ (if List.member player model.players then
                                        "player-select-circle--selected "

                                    else
                                        " "
                                   )
                                ++ playerClass player
                    in
                    div [ class "player-select-circle-container" ]
                        [ button
                            [ class buttonClass
                            , onClick (SelectPlayer player)
                            ]
                            []
                        ]
                )
                availablePlayers
            )
        , div
            [ class "action-button-container"
            ]
            (if List.length model.players >= 1 then
                [ button
                    [ onClick StartGame ]
                    [ text "START" ]
                ]

             else
                []
            )
        ]


playerClass : Player -> String
playerClass player =
    case player of
        Player class ->
            class


clock : Model -> Html Msg
clock model =
    div
        [ style "min-height" "100vh"
        , style "display" "flex"
        , style "flex-direction" "column"
        , style "justify-content" "center"
        , style "align-items" "center"
        ]
        [ div
            [ class "grid"
            ]
            (List.map
                (\player ->
                    let
                        buttonClass =
                            "player-select-circle "
                                ++ (if model.activePlayer == Just player then
                                        "player-select-circle--selected "

                                    else
                                        " "
                                   )
                                ++ playerClass player
                    in
                    div [ class "player-select-circle-container player-select-circle-container--large" ]
                        [ button
                            [ class buttonClass
                            , onClick (SetActivePlayer player)
                            ]
                            [ text
                                (Dict.get (playerClass player) model.times
                                    |> Maybe.withDefault 0
                                    |> secondsToString
                                )
                            ]
                        ]
                )
                model.players
            )
        , div
            [ style "text-align" "center"
            , class "action-button-container"
            ]
            (if model.activePlayer == Nothing then
                []

             else
                [ button
                    [ onClick Pause ]
                    [ text "PAUSE" ]
                ]
            )
        ]


secondsToString : Int -> String
secondsToString seconds =
    let
        s =
            modBy 60 seconds

        m =
            seconds // 60 |> modBy 60

        h =
            seconds // 3600
    in
    [ h, m, s ]
        |> List.map String.fromInt
        |> List.map (String.padLeft 2 '0')
        |> String.join ":"
