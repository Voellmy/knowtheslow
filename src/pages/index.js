import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import ElmHandler from "../components/elm-handler"
import { Elm } from "../Elm/Main.elm"

const IndexPage = () => {
  const flags = { getOrElse: () => {} }

  return (
    <Layout>
      <SEO title="Player Timer" keywords={[`boardgame`, `game`, `timer`, `time`, `clock`, `player`, `stopwatch`]} />

      <ElmHandler
        src={Elm.Main}
        ports={() => {}}
        flags={flags}
        options={flags}
      />
    </Layout>
  )
}

export default IndexPage
